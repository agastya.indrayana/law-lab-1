from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.views.generic import TemplateView
from .utils import ambil_resource, ambil_token

class LoginView(TemplateView):
    def get_template_names(self):
        return "authentication/login.html"
    
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        username = request.POST.get("username", None)
        password = request.POST.get("password", None)

        if username != None and password != None:
            response = ambil_token(username, password)
            access_token = response.get("access_token", None)
            request.session["access_token"] = access_token
            request.session.modified = True
            return HttpResponseRedirect('/')
        else:
            context["messege"] = "Username or Password is empty"
            return self.render_to_response(context=context)

def logout(request):
    request.session.flush()
    return HttpResponseRedirect('/')
