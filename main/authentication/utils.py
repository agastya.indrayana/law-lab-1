import requests
import json
from conf.settings import INFRALABS_ID, INFRALABS_SECRET
from django.contrib.auth.mixins import LoginRequiredMixin, AccessMixin


def ambil_token(user, pasw):
  URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/token"
  payload = {'username': user, 'password': pasw, 'grant_type': 'password', 'client_id': INFRALABS_ID, 'client_secret': INFRALABS_SECRET}
  r = requests.post(url = URL, data=payload)
  r = json.loads(r.text)
  return r

def ambil_resource(access_token):
  URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
  data_headers = { 'Authorization': 'Bearer ' + access_token }
  r = requests.get(url = URL, headers = data_headers)
  r = json.loads(r.text)
  return r

def cek_token_validity(access_token):
  URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
  data_headers = { 'Authorization': 'Bearer ' + access_token }
  r = requests.get(url = URL, headers = data_headers).status_code
  return r

class LoginRequiredMixin(AccessMixin):
  def dispatch(self, request, *args, **kwargs):
    # self.raise_exception = True
    
    if request.session.get("access_token", False):
      if cek_token_validity(request.session.get("access_token", False)) == 200:
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)
      else:
        return self.handle_no_permission()
    else:
      return self.handle_no_permission()
  