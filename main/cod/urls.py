from django.urls import path
from .views import CalcView

urlpatterns = [
    path('calc/', CalcView.as_view()),
]
