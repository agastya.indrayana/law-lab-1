from django.shortcuts import render
from django.views.generic import TemplateView
import requests
import json
from django.http import JsonResponse

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
 
@method_decorator(csrf_exempt, name='dispatch')
class CalcView(TemplateView):
    def get_template_names(self):
        return "cod/login.html"
    
    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        a = request.POST.get("a", 0)
        b = request.POST.get("b", 0)

        data = {
            "res" : int(a) + int(b)
        }

        return JsonResponse(data)

