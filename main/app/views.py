from django.shortcuts import render
from django.views.generic import TemplateView
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from authentication.utils import ambil_resource, LoginRequiredMixin
from conf.settings import MICROSERVICES
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.http import HttpResponse
import requests
from io import BytesIO
from datetime import datetime


class HomeView(TemplateView):
    def get_template_names(self):
        return "app/index.html"

    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        if self.request.session.get("access_token", False): 
            context["user_id"] = ambil_resource(self.request.session.get("access_token")).get("user_id")
        else:
            context["user_id"] = "Anonymous"
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        search_param = request.POST.get("search")
        context["search_param"] = search_param
        news = getNews(search_param)
        if news != None:
            context["news"] = news
        return self.render_to_response(context=context)

class NewsView(TemplateView):
    def get_template_names(self):
        return "app/news.html"

    def get_context_data(self, *args, **kwargs):
        context = super(NewsView, self).get_context_data(*args, **kwargs)
        if self.request.session.get("access_token", False): 
            context["user_id"] = ambil_resource(self.request.session.get("access_token")).get("user_id")
        else:
            context["user_id"] = "Anonymous"
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        search_param = request.POST.get("search")
        context["search_param"] = search_param
        news = getNews(search_param)
        if news != None:
            context["news"] = news
        return self.render_to_response(context=context)

class CompressView(LoginRequiredMixin, TemplateView):
    def get_template_names(self):
        return "app/compress.html"

    def get_context_data(self, *args, **kwargs):
        context = super(CompressView, self).get_context_data(*args, **kwargs)
        if self.request.session.get("access_token", False): 
            context["user_id"] = ambil_resource(self.request.session.get("access_token")).get("user_id")
        else:
            context["user_id"] = "Anonymous"
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

    def post(self, request, *args, **kwargs):
        if request.session.get("access_token", False):
            user_id = ambil_resource(self.request.session.get("access_token")).get("user_id")
            data = {"user_id":user_id}

            files = request.FILES.getlist('files')
            prc_files = []
            file_path = []
            for f in files:
                path = default_storage.save(f'{f.name}', ContentFile(f.read()))
                prc_files.append(("files", (open(path))))
                file_path.append(path)
                # path = default_storage.delete(f'{f.name}')

            r = requests.post(MICROSERVICES["COMPRESS"], files=prc_files, data=data)


            response = HttpResponse(content_type ="application/zip")
            response["Content-Disposition"] = f'attachment; filename="{datetime.now().strftime("%d-%m-%Y_%H-%M-%S")}.zip"'
            
            response.write(r.content)

            for f in files:
                default_storage.delete(f'{f.name}')

            return response

            
        else:    
            context = self.get_context_data(**kwargs)
            context["error"] = True
            context["error_messege"] = "You have to login first"
            return self.render_to_response(context=context)

class StorageView(LoginRequiredMixin, TemplateView):
    def get_template_names(self):
        return "app/storage.html"

    def get_context_data(self, *args, **kwargs):
        context = super(StorageView, self).get_context_data(*args, **kwargs)
        if self.request.session.get("access_token", False): 
            context["user_id"] = ambil_resource(self.request.session.get("access_token")).get("user_id")
            r = requests.post(f'{MICROSERVICES["STORAGE_MICROSERVICES"]}user/{context["user_id"]}/', data={context["user_id"]})
            r = json.dumps(r.content)
        else:
            context["user_id"] = "Anonymous"
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

class MetadataView(TemplateView):
    def get_template_names(self):
        return "app/metadata.html"

    def get_context_data(self, *args, **kwargs):
        context = super(MetadataView, self).get_context_data(*args, **kwargs)
        if self.request.session.get("access_token", False): 
            context["user_id"] = ambil_resource(self.request.session.get("access_token")).get("user_id")
        else:
            context["user_id"] = "Anonymous"
        return context

    def get(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

    def post(self, request, *args, **kwargs):
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context=context)

def getNews(param):
    api_key = "ETzDM8UC2axfZ9G0m4jFkGoeMBvDiPzG"
    resp = requests.get(
        f'https://api.nytimes.com/svc/search/v2/articlesearch.json?q={param}&api-key={api_key}')
    if resp.status_code == 200:
        res = []
        resp = resp.json()
        news = resp["response"]["docs"]
        for item in news:
            data = {
                "abstract": item["abstract"],
                "headline": item["headline"]["main"],
                "web_url": item["web_url"]
            }
            if item.get("multimedia", None) != None and len(item["multimedia"]) != 0:
                img_url = item["multimedia"][0]["url"]
                data["img_url"] = f'https://static01.nyt.com/{img_url}'
                data["img_exist"] = True
            else:
                data["img_exist"] = False
            res.append(data)
        return res
    else:
        return None
