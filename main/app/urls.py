from django.urls import path
from .views import HomeView, NewsView, CompressView, MetadataView, StorageView

urlpatterns = [
    path('', HomeView.as_view()),
    path('news/', NewsView.as_view()),
    path('compress/', CompressView.as_view()),
    path('storage/', MetadataView.as_view()),
    path('metadata/', StorageView.as_view()),
]
