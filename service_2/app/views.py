from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse, HttpResponseForbidden, HttpResponseBadRequest
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from .models import FileStorage

@method_decorator(csrf_exempt, name='dispatch')
class StoreFileView(View):
    def post(self, request):
        files = request.FILES.get('files', None)
        user_id = request.POST.get("user_id", None)
        if files ==None or user_id == None:
            return HttpResponseBadRequest("missing file or user_id")
        else:
            obj = FileStorage(user_id=user_id,files=files)
            obj.save()
            return HttpResponse(status=201)

@method_decorator(csrf_exempt, name='dispatch')
class RetriveFileView(View):
    def post(self, request, file_id):
        user_id = request.POST.get("user_id", None)
        file_record = get_object_or_404(FileStorage,id = file_id)
        if user_id != file_record.user_id:
            return HttpResponseForbidden("you're not allowed to access other people file")
        else:
            filename = file_record.files.name.split('/')[-1]
            response = HttpResponse(file_record.files, content_type='text/plain')
            response['Content-Disposition'] = 'attachment; filename=%s' % filename
            return response


@method_decorator(csrf_exempt, name='dispatch')
class UserFileListView(View):
    def post(self, request, user_id):
        if request.POST.get("user_id", None) != str(user_id):
            return HttpResponseForbidden("you're not allowed to access other people file") 
        files = FileStorage.objects.filter(user_id = user_id)
        prc_file = []
        for file_record in files:
            data = {
                "file_name" : file_record.files.name.split('/')[-1],
                "date_added" : file_record.timestamp.strftime("%d-%m-%Y %H-%M-%S"),
                "id" : f'{file_record.id}'
            }
            prc_file.append(data)
        return JsonResponse({"data":prc_file})

