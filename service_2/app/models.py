from django.db import models

class FileStorage(models.Model):
    user_id = models.CharField(max_length=30)
    timestamp = models.DateTimeField(auto_now_add=True)
    files = models.FileField(upload_to='file_storage')