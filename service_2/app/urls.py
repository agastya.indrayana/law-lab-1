from django.urls import path
from .views import StoreFileView, RetriveFileView, UserFileListView

urlpatterns = [
    path('storage/', StoreFileView.as_view()),
    path('storage/file/<int:file_id>/', RetriveFileView.as_view()),
    path('storage/user/<int:user_id>/', UserFileListView.as_view()),
]
