from django import forms
from .models import FileStorage

class FileUploadForm(forms.ModelForm):
    class Meta:
        model = FileStorage
        include = ["files"]

    def __init__(self, *args, **kwargs):
        super(FileUploadForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'