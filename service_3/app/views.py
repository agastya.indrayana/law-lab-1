from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse, HttpResponseForbidden, HttpResponseBadRequest
from django.views import View
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from .models import Metadata

@method_decorator(csrf_exempt, name='dispatch')
class MetadataView(View):
    def post(self, request):
        metadata = request.POST.get('metadata', None)
        user_id = request.POST.get("user_id", None)
        if metadata ==None or user_id == None:
            return HttpResponseBadRequest("missing metadata or user_id")
        else:
            obj = Metadata(user_id=user_id,metadata=metadata)
            obj.save()
            return HttpResponse(status=201)

@method_decorator(csrf_exempt, name='dispatch')
class RetriveMetadataView(View):
    def post(self, request, metadata_id):
        user_id = request.POST.get("user_id", None)
        metadata = get_object_or_404(Metadata,id = metadata_id)
        if user_id != metadata.user_id:
            return HttpResponseForbidden("you're not allowed to access other people metadata_id")
        else:
            data = {
                "id":metadata.id,
                "user_id":metadata.user_id,
                "timestamp":metadata.timestamp,
                "metadata":metadata.metadata,
            }
            response = JsonResponse({"data":data})
            return response


@method_decorator(csrf_exempt, name='dispatch')
class UserMetadataListView(View):
    def post(self, request, user_id):
        if request.POST.get("user_id", None) != str(user_id):
            return HttpResponseForbidden("you're not allowed to access other people file") 
        metadatas = Metadata.objects.filter(user_id = user_id)
        prc_metadata = []
        for metadata in metadatas:
            data = {
                "id":metadata.id,
                "user_id":metadata.user_id,
                "timestamp":metadata.timestamp,
                "metadata":metadata.metadata,
            }
            prc_metadata.append(data)
        return JsonResponse({"data":prc_metadata})

