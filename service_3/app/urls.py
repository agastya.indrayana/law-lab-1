from django.urls import path
from .views import MetadataView, RetriveMetadataView, UserMetadataListView

urlpatterns = [
    path('metadata/', MetadataView.as_view()),
    path('metadata/metadata/<int:metadata_id>/', RetriveMetadataView.as_view()),
    path('metadata/user/<int:user_id>/', UserMetadataListView.as_view()),
]
