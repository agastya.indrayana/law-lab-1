from django.db import models

class Metadata(models.Model):
    user_id = models.CharField(max_length=30)
    timestamp = models.DateTimeField(auto_now_add=True)
    metadata = models.TextField()