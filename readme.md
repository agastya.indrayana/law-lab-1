# LAB LAW

# ASSIGNMENT 1: Setup Web App & Calling Service (REST)

## Installation

make sure you have pipenv installed

```bash
pipenv update
pipenv shell
python3 mananage.py migrate
python3 mananage.py runserver
```

## Usage

This web app utilize the The New York Times to allow user to search for news stories, read the headline and see any cover photo and read the full article on The New York Times website by providing links to each news stories.

## Implementation Detail

User fill an input field and pressed the search button, that action send a POST request to the application with user search param as the payload. Then the app make API call to The New York Times API service asking for news with user param as a query.

```python
def getNews(param):
    api_key = "ETzDM8UC2axfZ9G0m4jFkGoeMBv***"
    resp = requests.get(f'https://api.nytimes.com/svc/search/v2/articlesearch.json?q={param}&api-key={api_key}')
```

Result are then sent to template inside context to be rendered.

## Additional information

### URL Server REST
https://api.nytimes.com/svc/search/v2/articlesearch.json?q={param}&api-key={api_key}

### URL Server WEB
http://152.118.148.95:20672/


# ASSIGNMENT 2: Microservices & CRUD Storage
# Installation

make sure you have pipenv installed

```bash
pipenv update
pipenv shell
```

Each service need to be run at port 20674-20677 starting from main, service_1 until service_3

## Usage

### Main
Serve as the main app that handle authentication and communicate with other microservices.

### service_1
A Microservice that when contacted with the appropriate credential and files, will return said files inside a zip file.
endpoint is http://localhost:20675/api/compress/ POST method with access_token and files as form-data.
![File compression failed because of expired token](/images/compressfile-expired_access_token.PNG)
![File compression succeded](/images/compressfile-success.PNG)

### service_2
A Microservice that when contacted with the appropriate credential and request param, will save or retrive file to and from server.
Create endpoint is http://localhost:20676/api/storage/ POST method with user_id and file as form-data.
Retrive endpoint is http://localhost:20676/api/storage/file/<int:file_id> POST method with user_id as form-data.
Retrive user's files endpoint is http://localhost:20676/api/storage/user/<int:user_id> POST method with user_id as form-data.
![Save file](/images/save_file.PNG)
![Download saved file](/images/download_file.PNG)
![retrive all of user's stored files](/images/list_user_file.PNG)

### service_3
A Microservice that when contacted with the appropriate credential and request param, will save or retrive metadata to and from server
Create endpoint is http://localhost:20677/api/metadata/ POST method with user_id and metadata as form-data.
Retrive endpoint is http://localhost:20676/api/metadata/file/<int:metadata_id> POST method with user_id as form-data.
Retrive user's metadata endpoint is http://localhost:20676/api/metadata/user/<int:user_id> POST method with user_id as form-data.
![Save metadata](/images/save_metadata.PNG)
![Download saved metadata](/images/get_metadata.PNG)
![retrive all of user's stored metadatas](/images/get_user_metadata.PNG)

## Implementation Detail

user are required to signin to generate access token. access token are then stored inside user session. The presence of access token inside user session and the validity(not expired yet) of said token will be the indication if a user has login or not.
 Logout process flush user's session clean.


## Additional information

### URL Server WEB
http://152.118.148.95:20674/




## License
[MIT](https://choosealicense.com/licenses/mit/)

