from django.db import models

class CompressionJob(models.Model):
    user_id = models.CharField(max_length=30)
    timestamp = models.DateTimeField(auto_now_add=True)
    files = models.TextField()