from django.urls import path
from .views import CompressFileView

urlpatterns = [
    path('compress/', CompressFileView.as_view()),
]
