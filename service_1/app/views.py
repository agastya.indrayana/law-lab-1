from django.shortcuts import render
from django.http import JsonResponse, HttpResponseRedirect, HttpResponse, HttpResponseForbidden, HttpResponseBadRequest
from django.views import View
import json
from io import BytesIO
import os

from django.core.files.storage import default_storage
from django.core.files.base import ContentFile
from django.conf import settings

from wsgiref.util import FileWrapper

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from zipfile import ZipFile
from datetime import datetime

import requests


@method_decorator(csrf_exempt, name='dispatch')
class CompressFileView(View):
    def post(self, request):
        files = request.FILES.getlist('files')
        access_token = request.POST.get("access_token")
        if access_token == None or access_token == "":
            return HttpResponseForbidden("You have to include your access_token")
        elif cek_token_validity(access_token) != 200:
            return HttpResponseForbidden("something wrong with your access_token, maybe you need a fresh access_token")
        in_memory = BytesIO()
        zip = ZipFile(in_memory, "w")
            
        for f in files:
            path = default_storage.save(f'{f.name}', ContentFile(f.read()))
            zip.write(path)
            path = default_storage.delete(f'{f.name}')
        
        # fix for Linux zip files read in Windows
        for file in zip.filelist:
            file.create_system = 0    
            
        zip.close()

        response = HttpResponse(content_type ="application/zip")
        response["Content-Disposition"] = f'attachment; filename="{datetime.now().strftime("%d-%m-%Y_%H-%M-%S")}.zip"'
        
        in_memory.seek(0)    
        response.write(in_memory.read())
        
        return response

def ambil_resource(access_token):
  URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
  data_headers = { 'Authorization': 'Bearer ' + access_token }
  r = requests.get(url = URL, headers = data_headers)
  r = json.loads(r.text)
  return r

def cek_token_validity(access_token):
  URL = "http://oauth.infralabs.cs.ui.ac.id/oauth/resource"
  data_headers = { 'Authorization': 'Bearer ' + access_token }
  r = requests.get(url = URL, headers = data_headers).status_code
  return r